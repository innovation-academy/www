{ h, Component } = require('preact')
{ Router } = require('preact-router')

Header = require('./header').default
Home = require('./home').default
Profile = require('./profile').default

class App extends Component
  # Gets fired when the route changes.
  constructor: () ->
    @currentUrl = ''

  handleRoute: (e) =>
    @currentUrl = e.url

  render: () =>
    h 'main', id:"app",
      h Header
      h Router, onChange: @handleRoute,
        h Home, path: "/"
        h Profile, path: "/profile/", user: ""
        h Profile, path: "/profile/:user"

module.exports = App
