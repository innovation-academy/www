import { h, Component } from 'preact';
import { Link } from 'preact-router';
import style from './style';

export default class Header extends Component {
  render() {
    return (
      <header class={style.header}>
        <h1>Innovation Academy</h1>
        <nav>
          <Link href="/">Home</Link>
          <Link href="/profile">Profile</Link>
          <Link href="/profile/Caleb">Caleb</Link>
          <Link href="/profile/Gage">Gage</Link>
          <Link href="/profile/Sam">Sam</Link>
        </nav>
      </header>
    );
  }
}
