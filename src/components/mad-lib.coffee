{ h, Component } = require('preact')
{ route } = require('preact-router')

module.exports = class extends Component
  
  constructor: (props) ->

  handleClick: (e) =>
    alert("Hi, #{@state.name}")

  updateName: (e) =>
    console.log 'hi'
    @name = name

  updateText: (e) =>
    @setState({ name: e.target.value })
    route("/profile/#{e.target.value}")

  render: ->
    name = @state.name || @props.name
    mad_lib_title = h 'h1', {}, "For #{name}, a Mad Lib"

    mad_lib_words = {
      ing_word: "Word ending with 'ing'"
      ed_word_1: "Word ending with 'ed'"
      ed_word_2: "Word ending with 'ed'"
      verb: 'Verb'
      noun_1: 'Noun'
      noun_2: 'Noun'
      noun_3: 'Noun'
      noun_4: 'Noun'
      place: 'Place'
      animal: 'Animal'
      number: 'Number'
    }

    mlw = mad_lib_words

    mad_lib_contents = h 'p', {}, "#{name} and I were #{@state.ing_word} along the sidewalk when an alien #{@state.ed_word_1} me. I was #{@state.ed_word_2} into their #{@state.noun_1} and it blasted off. Then the alien asked me to #{@state.verb} on the TV. I was suprised they spoke English. the aliens had a pet #{@state.animal}. We ordered a #{@state.noun_2} and it tasted good. As we came back into the galaxy, one alien asked me if I wanted a #{@state.noun_3}. I said no but I would like a #{@state.noun_4}. He got it for me and then dropped me off at my #{@state.place}. Then I realized I had been gone for #{@state.number} years!"

    mad_lib_inputs = []
    for word, contents of mad_lib_words
      mad_lib_inputs.push h 'input',
      value: @state[word] or '',
      type: if word is 'number' then 'number' else 'text'
      onInput: @linkState word
      placeholder: contents

    mad_lib_ready = true
    for word of mad_lib_words
      if @state[word] is undefined
        mad_lib_ready = false

    header = h 'h1', {}, "Hello, #{name}"
    contents = h 'div', id: "foo",
      # h 'input', placeholder: 'Name', value: name, onInput: @linkState 'name'

      # The above and below solutions do the same thing

      h 'input', id: 'name', placeholder: 'Enter your name', value: name, onInput: @updateText
      if name then header
      h 'button', onClick: @handleClick, "Click me to alert(\"Hi, #{name}\")"
      if name then mad_lib_title
      if name then mad_lib_inputs
      if name and mad_lib_ready then mad_lib_contents
