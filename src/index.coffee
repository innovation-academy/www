# import 'lie'
# import 'isomorphic-fetch'
{ h, render } = require('preact')
require './style'

root = undefined

init = ->
  App = require('./components/app.coffee')
  # root = render(h(App), document.body, root)
  render(h(App), document.body)

init()

if (module.hot)
  module.hot.accept('./components/app.coffee', () => requestAnimationFrame( () =>
    flushLogs()
    init()
) )
  # optional: mute HMR/WDS logs
  log = console.log
  logs = []
  console.log = (t, args...) =>
    if typeof t is 'string' and t.match(/^\[(HMR|WDS)\]/)
      if t.match(/(up to date|err)/i)
        logs.push(t.replace(/^.*?\]\s*/m,''), args...)

    else
      log.call(console, t, args...)

  flushLogs = () =>
    console.log("%c🚀 #{logs.splice(0,logs.length).join(' ')}", 'color:#888')

