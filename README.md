# Innovation Academy Website

This is a website for FHS Innovation Academy. http://iasummer.netlify.com/. At this point it is mostly a Preact test.

# Quick-Start Guide

- [Installation](#installation)
- [Development Workflow](#development-workflow)
- [Structure](#structure)
- [CSS Modules](#css-modules)
- [Handling URLS](#handling-urls)


## Installation

**1. Clone this repo:**

```sh
git clone --depth 1 https://gitlab.com/innovation-academy/www
cd www
```

**3. Install the dependencies:**

```sh
npm install -g yarn
yarn
```

> You're done installing! Now let's get started developing.


## Development Workflow


**4. Start a live-reload development server:**

```sh
npm run dev
```

> This is a full web server nicely suited to your project. Any time you make changes within the `src` directory, it will rebuild and even refresh your browser.

**5. Testing with `mocha`, `karma`, `chai`, `sinon` via `phantomjs`:**

```sh
npm test
```

**6. Generate a production build in `./build`:**

```sh
npm run build
```

---


## Structure

Apps are built up from simple units of functionality called Components. A Component is responsible for rendering a small part of an application, given some input data called `props`, generally passed in as attributes in CoffeeScript. A component can be as simple as:

```coffee
class Link extends Component {
  render({ to, children }) ->
    h 'a', href: to, children

# usage:
h Link, to: "/", Home
```


---


## CSS Modules

> **Warning:** This will soon be removed and be replaced with good ol' [stylus](http://stylus-lang.com/).

This project is set up to support [CSS Modules](https://github.com/css-modules/css-modules).  By default, styles in `src/style` are **global** (not using CSS Modules) to make global declarations, imports and helpers easy to declare.  Styles in `src/components` are loaded as CSS Modules via [Webpack's css-loader](https://github.com/webpack/css-loader#css-modules).  Modular CSS namespaces class names, and when imported into JavaScript returns a mapping of canonical (unmodified) CSS classes to their local (namespaced/suffixed) counterparts.

When imported, this LESS/CSS:

```css
.redText { color:red; }
.blueText { color:blue; }
```

... returns the following map:

```js
import styles from './style.css';
console.log(styles);
// {
//   redText: 'redText_local_9gt72',
//   blueText: 'blueText_local_9gt72'
// }
```

Note that the suffix for local classNames is generated based on an md5 hash of the file. Changing the file changes the hash.


---


## Handling URLS

:information_desk_person: This project contains [URL routing](http://git.io/preact-router).

Pages are just regular components that get mounted when you navigate to a certain URL. Any URL parameters get passed to the component as `props`.

Defining what component(s) to load for a given URL is easy and declarative. You can even mix-and-match URL parameters and normal props.

```coffee
h Router
  h A, path: "/"
  h B, path: "/b", id: "42"
  h C, path: "/c/:id"
```


---


## License

MIT


[Preact]: https://developit.github.io/preact
[webpack]: https://webpack.github.io
